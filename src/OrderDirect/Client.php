<?php
namespace OrderDirect;

use Guzzle\Http\Client as HTTPClient;
use Guzzle\Http\Message\RequestInterface;
use OrderDirect\Entity\EntityInterface;

class Client
{

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var string
     */
    private $rawResponse;

    /**
     * @var string
     */
    private $authKey;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function login()
    {
        $url = $this->connection->getLoginPath();
        $httpClient = new HTTPClient();

        $loginResponse = $httpClient->get($url)->send();
        $auth = json_decode($loginResponse->getBody(), true);

        if (isset($auth['data']['session']['key'])) {
            $this->authKey = $auth['data']['session']['key'];
        } else {
            throw new \Exception($auth['error']['code'] . ' : ' . $auth['error']['message']);
        }
    }

    public function logout()
    {
        $url = $this->connection->getLogoutPath($this->authKey);
        $httpClient = new HTTPClient();

        $logoutResponse = $httpClient->get($url)->send();
    }

    /**
     * @return bool
     */
    public function isUp()
    {
        $url = $this->connection->getStatusPath();
        $httpClient = new HTTPClient();

        $statusResponse = $httpClient->get($url)->send();

        $status = json_decode($statusResponse, true);

        return ($status['state'] == 'Running');
    }

    public function getRawResponse()
    {
        return $this->rawResponse;
    }

    public function getRelatie($nummer)
    {
        return $this->connection->get(sprintf('getPartyData/0/%s', $nummer));
    }

    public function getRelatieProperties($nummer)
    {
        return $this->connection->get(sprintf('getPartyProperties/0/%s', $nummer));
    }

    /**
     * @param $type
     * @param $entity
     * @return int
     */
    public function create($type, $entity)
    {
        $url = $this->connection->getPutPath($type, $this->authKey);
        $httpClient = new HTTPClient();
        $data = [];
        $data['data'][$type] = $entity->toArray();

        $response = $httpClient->put($url,[], json_encode($data))->send();
        $responseData = json_decode($response->getBody(), true);

        $this->saveLog($type, 'create' , $data, $responseData);

        return isset($responseData['data'][$type]['number']) ? $responseData['data'][$type]['number'] :$entity->getNumber();
    }

    /**
     * @param $type
     * @param EntityInterface $entity
     * @return int
     */
    public function update($type, EntityInterface $entity)
    {
        $url = $this->connection->getPostPath($type, $this->authKey, $entity->getNumber());
        $httpClient = new HTTPClient();
        $data = [];
        $data['data'][$type] = $entity->toArray();

        $response = $httpClient->post($url,[], json_encode($data))->send();

        $responseData = json_decode($response->getBody(), true);
        $this->saveLog($type, 'update' , $responseData);

        return $entity->getNumber();
    }

    public function get($type, $id)
    {
        $url = $this->connection->getPath($type, $this->authKey, $id);
        $httpClient = new HTTPClient();

        $response = $httpClient->get($url)->send();
        $record = json_decode($response->getBody(), true);

        if(!isset($record['data'])){
            throw new \Exception('Record not exists');
        };

        $this->saveLog($type, 'get' , ['id' => $id], $record['data']);

        return $record['data'];
    }

    private function saveLog($type, $methode, $requestData = [], $responseData = []) {
        $data['request'] = $requestData;
        $data['response'] = $responseData;
        $data['methode'] = $methode;

        file_put_contents('/tmp/' . date("Ymdh-") . $type . '.' . $methode . '.log', json_encode($data));
    }

    public function search($type, $params)
    {

    }

    public function delete($type, $id)
    {

    }
} 

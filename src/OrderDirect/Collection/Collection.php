<?php
namespace OrderDirect\Collection;


abstract class Collection
{
    protected $position = 0;

    /**
     * @var array
     */
    protected $entities = [];

    public function __construct($products)
    {
        $this->entities = $products;
        $this->position = 0;
    }

    public function next()
    {
        $this->position++;
    }

    public function key()
    {
        return $this->position;
    }

    public function valid()
    {
        return isset($this->entities[$this->position]);
    }

    public function rewind()
    {
        $this->position = 0;
    }
}

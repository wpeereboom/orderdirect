<?php
namespace OrderDirect\Collection;

use Iterator;
use OrderDirect\Entity\Product;

class ProductCollection extends Collection implements Iterator
{
    public function current()
    {
        return new Product($this->entities[$this->position]);
    }
}

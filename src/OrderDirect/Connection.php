<?php
namespace OrderDirect;


class Connection
{

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $protocol;

    /**
     * @var int
     */
    private $port;

    /**
     * @var \Guzzle\Http\Client
     */
    private $httpClient;

    /**
     * @var string
     */
    private $basePath;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    public function __construct($host, $username, $password, $basePath, $protocol = 'http', $port = 8080)
    {

        $this->host = $host;
        $this->protocol = $protocol;
        $this->port = $port;
        $this->basePath = $basePath;

        $this->username = $username;
        $this->password = $password;
    }

    public function getBaseUrl()
    {
        return $this->protocol . "://" . $this->host . ":" . $this->port . $this->basePath;
    }

    /**
     * @param string $type
     * @param string $key
     * @param int $id
     * @return string
     */
    public function getPath($type, $key, $id)
    {
        return $this->getBaseUrl() . $type . '/' . $key . '/' . $id . '/json';
    }

    public function getPostPath($type, $key, $id)
    {
        return $this->getBaseUrl() . $type . '/' . $key . '/' . $id;
    }

    public function getPutPath($type, $key)
    {
        return $this->getBaseUrl() . $type  . '/' . $key;
    }

    public function getLoginPath()
    {
        return $this->getBaseUrl() . 'logIn/1/' . $this->username . '/' . $this->password . '/json';
    }

    /**
     * @param $key
     * @return string
     */
    public function getLogoutPath($key)
    {
        return $this->getBaseUrl() . 'logOut/' . $key . '/json';
    }

    /**
     * @return string
     */
    public function getStatusPath()
    {
        return $this->getBaseUrl() . 'status/json';
    }
} 

<?php
namespace OrderDirect\Entity;

class Contact implements EntityInterface
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $index;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var Email
     */
    private $email = null;

    /**
     * @var string
     */
    private $active;

    /**
     * @var string
     */
    private $accountsPayable;

    /**
     * @var string
     */
    private $mailings;

    /**
     * @var array
     */
    private $columns = ['id', 'index', 'name', 'gender', 'active', 'accountsPayable', 'mailings'];

    public function __construct($data)
    {
        foreach($this->columns as $column) {
            if(isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }

        if (isset($data['email'])) {
            $this->email = new Email($data['email']);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            $data[$column] = $this->$column;
        }

        if(!is_null($this->email)) {
            $data['email'] = $this->email->toArray();
        }

        return $data;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return string
     */
    public function getAccountsPayable()
    {
        return $this->accountsPayable;
    }

    /**
     * @return string
     */
    public function getMailings()
    {
        return $this->mailings;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param int $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @param Email $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param string $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @param string $accountsPayable
     */
    public function setAccountsPayable($accountsPayable)
    {
        $this->accountsPayable = $accountsPayable;
    }

    /**
     * @param string $mailings
     */
    public function setMailings($mailings)
    {
        $this->mailings = $mailings;
    }

}

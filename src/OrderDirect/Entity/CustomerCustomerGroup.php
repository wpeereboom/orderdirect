<?php
namespace OrderDirect\Entity;

class CustomerCustomerGroup implements EntityInterface
{

    /**
     * @var CustomerGroup
     */
    private $customerGroup = null;

    /**
     * @var int
     */
    private $index;

    /**
     * @var array
     */
    private $columns =  []; // ['index',];

    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }

        if (isset($data['customerGroup'])) {
            $this->customerGroup = new CustomerGroup($data['customerGroup']);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            $data[$column] = $this->$column;
        }

        if(!is_null($this->customerGroup)) {
            $data['customerCustomerGroup']['customerGroup']  = $this->customerGroup->toArray();
        }

        return $data;
    }

    /**
     * @return CustomerGroup
     */
    public function getCustomerGroup()
    {
        return $this->customerGroup;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }
}

<?php
namespace OrderDirect\Entity;

class CustomerDepartment implements EntityInterface
{
    /**
     * @var Department[]
     */
    private $departments = [];

    public function __construct($data)
    {

        foreach($data as $departmentData) {
            $this->departments[] = new Department($departmentData['department']);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];
        foreach($this->departments as $department) {
            $data['department'] = $department->toArray();
        }

        return $data;
    }
}

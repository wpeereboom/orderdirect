<?php
namespace OrderDirect\Entity;

class CustomerGroup implements EntityInterface
{
    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $description;

    /**
     * @var array
     */
    private $columns = ['number', 'description',];

    public function __construct($data)
    {
        foreach($this->columns as $column) {
            if(isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            $data[$column] = $this->$column;
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}

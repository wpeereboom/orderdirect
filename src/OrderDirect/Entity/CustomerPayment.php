<?php
namespace OrderDirect\Entity;

class CustomerPayment implements EntityInterface
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $period;

    /**
     * @var array
     */
    private $columns = ['method', 'period'];

    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            $data[$column] = $this->$column;
        }

        return $data;
    }

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @param string $period
     */
    public function setPeriod($period)
    {
        $this->period = $period;
    }
}

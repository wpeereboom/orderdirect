<?php
namespace OrderDirect\Entity;

class Email implements EntityInterface
{
    /**
     * @var string
     */
    private $address;

    /**
     * @var array
     */
    private $columns = ['address'];

    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            $data[$column] = $this->$column;
        }

        return $data;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
}

<?php
namespace OrderDirect\Entity;

interface EntityInterface
{
    public function toArray();
}

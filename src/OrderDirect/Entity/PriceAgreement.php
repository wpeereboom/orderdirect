<?php
namespace OrderDirect\Entity;

class PriceAgreement implements EntityInterface
{
    /**
     * @var int
     */
    private $apply;

    /**
     * @var array
     */
    private $columns = ['apply'];

    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            $data[$column] = $this->$column;
        }

        return $data;
    }

    /**
     * @return int
     */
    public function getApply()
    {
        return $this->apply;
    }

    /**
     * @param int $apply
     */
    public function setApply($apply)
    {
        $this->apply = $apply;
    }
}

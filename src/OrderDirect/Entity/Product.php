<?php
namespace OrderDirect\Entity;

class Product implements EntityInterface
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $isAdditionalDescription;

    /**
     * @var string
     */
    private $stockControl;

    /**
     * @var string
     */
    private $applyPriceConditions;

    /**
     * @var string
     */
    private $invoiceRebates;

    /**
     * @var string
     */
    private $productLists;

    /**
     * @var string
     */
    private $pickingLists;

    /**
     * @var string
     */
    private $defaultSalesPrice;

    /**
     * @var string
     */
    private $defaultPurchasePrice;

    /**
     * @var string
     */
    private $salesPriceSupplierIndex;

    /**
     * @var string
     */
    private $isProductConfiguration;

    /**
     * @var string
     */
    private $created = null;

    /**
     * @var string
     */
    private $modified = null;

    /**
     * @var BaseUnit
     */
    private $baseUnit = null;

    /**
     * @var VatGroup
     */
    private $vatGroup = null;

    /**
     * @var ProductGroup
     */
    private $productGroup = null;

    /**
     * @var Manufacturer
     */
    private $manufacturer = null;

    /**
     * @var ProductFieldType
     */
    private $productFieldType = null;

    /**
     * @var ProductSupplier[]
     */
    private $productSuppliers = null;

    /**
     * @var ProductStock[]
     */
    private $productStocks = null;

    /**
     * @var ProductField[]
     */
    private $productFields = null;

    /**
     * @var array
     */
    private $columns = [
        'code','description','isAdditionalDescription','stockControl',
        'applyPriceConditions','invoiceRebates', 'productLists', 'pickingLists','defaultSalesPrice',
        'defaultPurchasePrice', 'salesPriceSupplierIndex','isProductConfiguration',
        'created','modified'
    ];

    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }

        if (isset($data['productSuppliers']) && count($data['productSuppliers']) > 0) {
            foreach ($data['productSuppliers'] as $productSuppliersData) {
                $this->productSuppliers[] = new ProductSupplier($productSuppliersData['productSupplier']);
            }
        }

        if (isset($data['productStocks']) && count($data['productStocks']) > 0) {
            foreach ($data['productStocks'] as $productStocksData) {
                $this->productStocks[] = new ProductStock($productStocksData['productStock']);
            }
        }

        if (isset($data['productFields']) && count($data['productFields']) > 0) {
            foreach ($data['productFields'] as $productFieldsData) {
                $this->productFields[] = new ProductField($productFieldsData['productField']);
            }
        }

        if (isset($data['productFieldType'])) {
            $this->productFieldType = new ProductFieldType($data['productFieldType']);
        }

        if (isset($data['manufacturer'])) {
            $this->manufacturer = new Manufacturer($data['manufacturer']);
        }

        if (isset($data['productGroup'])) {
            $this->productGroup = new ProductGroup($data['productGroup']);
        }

        if (isset($data['vatGroup'])) {
            $this->vatGroup = new VatGroup($data['vatGroup']);
        }

        if (isset($data['baseUnit'])) {
            $this->baseUnit = new BaseUnit($data['baseUnit']);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach ($this->columns as $column) {
            if(!is_null($this->$column)) {
                $data[$column] = $this->$column;
            }
        }

        if(isset($data['defaultSalesPrice']) && $data['defaultSalesPrice'] != '') {
            $data['salesPrice'] = $data['defaultSalesPrice'];
        }

        if (count($this->productSuppliers) > 0) {
            $data['productSuppliers'] = [];

            foreach ($this->productSuppliers as $productSupplier) {
                $data['productSuppliers'][]['productSupplier'] = $productSupplier->toArray();
            }
        }

        if (count($this->productStocks) > 0) {
            $data['productStocks'] = [];

            foreach ($this->productStocks as $productStock) {
                $data['productStocks'][]['productStock'] = $productStock->toArray();
            }
        }

        if (count($this->productFields) > 0) {
            $data['productFields'] = [];

            foreach ($this->productFields as $productField) {
                $data['productFields'][]['productField'] = $productField->toArray();
            }
        }

        if (!is_null($this->productFieldType)) {
            $data['productFieldType'] = $this->productFieldType->toArray();
        }

        if (!is_null($this->manufacturer)) {
            $data['manufacturer'] = $this->manufacturer->toArray();
        }

        if (!is_null($this->productGroup)) {
            $data['productGroup'] = $this->productGroup->toArray();
        }

        if (!is_null($this->vatGroup)) {
            $data['vatGroup'] = $this->vatGroup->toArray();
        }

        if (!is_null($this->baseUnit)) {
            $data['baseUnit'] = $this->baseUnit->toArray();
        }

        return $data;
    }

    public function getNumber()
    {
        return $this->getCode();
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getIsAdditionalDescription()
    {
        return $this->isAdditionalDescription;
    }

    /**
     * @param string $isAdditionalDescription
     */
    public function setIsAdditionalDescription($isAdditionalDescription)
    {
        $this->isAdditionalDescription = $isAdditionalDescription;
    }

    /**
     * @return string
     */
    public function getStockControl()
    {
        return $this->stockControl;
    }

    /**
     * @param string $stockControl
     */
    public function setStockControl($stockControl)
    {
        $this->stockControl = $stockControl;
    }

    /**
     * @return string
     */
    public function getApplyPriceConditions()
    {
        return $this->applyPriceConditions;
    }

    /**
     * @param string $applyPriceConditions
     */
    public function setApplyPriceConditions($applyPriceConditions)
    {
        $this->applyPriceConditions = $applyPriceConditions;
    }

    /**
     * @return string
     */
    public function getInvoiceRebates()
    {
        return $this->invoiceRebates;
    }

    /**
     * @param string $invoiceRebates
     */
    public function setInvoiceRebates($invoiceRebates)
    {
        $this->invoiceRebates = $invoiceRebates;
    }

    /**
     * @return string
     */
    public function getProductLists()
    {
        return $this->productLists;
    }

    /**
     * @param string $pickingLists
     */
    public function setProductLists($productLists)
    {
        $this->productLists = $productLists;
    }

    /**
     * @return string
     */
    public function getPickingLists()
    {
        return $this->pickingLists;
    }

    /**
     * @param string $pickingLists
     */
    public function setPickingLists($pickingLists)
    {
        $this->pickingLists = $pickingLists;
    }

    /**
     * @return string
     */
    public function getDefaultSalesPrice()
    {
        return $this->defaultSalesPrice;
    }

    /**
     * @param string $defaultSalesPrice
     */
    public function setDefaultSalesPrice($defaultSalesPrice)
    {
        $this->defaultSalesPrice = $defaultSalesPrice;
    }

    /**
     * @return string
     */
    public function getDefaultPurchasePrice()
    {
        return $this->defaultPurchasePrice;
    }

    /**
     * @param string $defaultPurchasePrice
     */
    public function setDefaultPurchasePrice($defaultPurchasePrice)
    {
        $this->defaultPurchasePrice = $defaultPurchasePrice;
    }

    /**
     * @return string
     */
    public function getSalesPriceSupplierIndex()
    {
        return $this->salesPriceSupplierIndex;
    }

    /**
     * @param string $salesPriceSupplierIndex
     */
    public function setSalesPriceSupplierIndex($salesPriceSupplierIndex)
    {
        $this->salesPriceSupplierIndex = $salesPriceSupplierIndex;
    }

    /**
     * @return string
     */
    public function getIsProductConfiguration()
    {
        return $this->isProductConfiguration;
    }

    /**
     * @param string $isProductConfiguration
     */
    public function setIsProductConfiguration($isProductConfiguration)
    {
        $this->isProductConfiguration = $isProductConfiguration;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param string $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return BaseUnit
     */
    public function getBaseUnit()
    {
        return $this->baseUnit;
    }

    /**
     * @return VatGroup
     */
    public function getVatGroup()
    {
        return $this->vatGroup;
    }

    /**
     * @return ProductGroup
     */
    public function getProductGroup()
    {
        return $this->productGroup;
    }

    /**
     * @return Manufacturer
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * @return ProductFieldType
     */
    public function getProductFieldType()
    {
        return $this->productFieldType;
    }

    /**
     * @return ProductSupplier[]
     */
    public function getProductSuppliers()
    {
        return $this->productSuppliers;
    }

    /**
     * @return ProductStock[]
     */
    public function getProductStocks()
    {
        return $this->productStocks;
    }

    /**
     * @return ProductField[]
     */
    public function getProductFields()
    {
        return $this->productFields;
    }

}

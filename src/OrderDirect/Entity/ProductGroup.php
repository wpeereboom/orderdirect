<?php
namespace OrderDirect\Entity;

class ProductGroup implements EntityInterface
{
    /**
     * @var int
     */
    private $number;

    /**
     * @var string
     */
    private $description;

    /**
     * @var array
     */
    private $columns = ['number', 'description'];

    /**
     * Manufacturer constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            $data[$column] = $this->$column;
        }

        return $data;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}

<?php
namespace OrderDirect\Entity;

class ProductStock implements EntityInterface
{
    /**
     * @var Branch
     */
    private $branch = null;

    /**
     * @var int
     */
    private $id = null;

    /**
     * @var string
     */
    private $minStock = null;

    /**
     * @var string
     */
    private $maxStock = null;

    /**
     * @var string
     */
    private $stock = null;

    /**
     * @var array
     */
    private $columns = ['id', 'minStock','maxStock', 'stock'];

    /**
     * Manufacturer constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }

        if (isset($data['branch'])) {
            $this->branch = new Branch($data['branch']);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            if(!is_null($this->$column)) {
                $data[$column] = $this->$column;
            }
        }

        if (!is_null($this->branch)) {
            $data['branch'] = $this->branch->toArray();
        }

        return $data;
    }

    /**
     * @return Branch
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param Branch $branch
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getMinStock()
    {
        return $this->minStock;
    }

    /**
     * @param string $minStock
     */
    public function setMinStock($minStock)
    {
        $this->minStock = $minStock;
    }

    /**
     * @return string
     */
    public function getMaxStock()
    {
        return $this->maxStock;
    }

    /**
     * @param string $maxStock
     */
    public function setMaxStock($maxStock)
    {
        $this->maxStock = $maxStock;
    }

    /**
     * @return string
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param string $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }
}

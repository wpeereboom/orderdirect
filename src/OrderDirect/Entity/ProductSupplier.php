<?php
namespace OrderDirect\Entity;

class ProductSupplier implements EntityInterface
{

    /**
     * @var int
     */
    private $index;

    /**
     * @var string
     */
    private $productNumber;

    /**
     * @var string
     */
    private $priceChangeDate;

    /**
     * @var string
     */
    private $originalPurchasePrice;

    /**
     * @var string
     */
    private $salesPrice;

    /**
     * @var string
     */
    private $minimumPurchaseQuantity = null;

    /**
     * @var string
     */
    private $purchasePrice;

    /**
     * @var Supplier
     */
    private $supplier = null;


    /**
     * @var array
     */
    private $columns = ['index', 'productNumber','originalPurchasePrice','salesPrice','purchasePrice','minimumPurchaseQuantity'];

    /**
     * Manufacturer constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }

        if (isset($data['supplier'])) {
            $this->supplier = new Supplier($data['supplier']);
        }

    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            if(!is_null($this->$column)) {
                $data[$column] = $this->$column;
            }
        }

        if (!is_null($this->supplier)) {
            $data['supplier'] = $this->supplier->toArray();
        }

        return $data;
    }

    /**
     * @return int
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param int $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    /**
     * @return string
     */
    public function getProductNumber()
    {
        return $this->productNumber;
    }

    /**
     * @param string $productNumber
     */
    public function setProductNumber($productNumber)
    {
        $this->productNumber = $productNumber;
    }

    /**
     * @return string
     */
    public function getPriceChangeDate()
    {
        return $this->priceChangeDate;
    }

    /**
     * @param string $priceChangeDate
     */
    public function setPriceChangeDate($priceChangeDate)
    {
        $this->priceChangeDate = $priceChangeDate;
    }

    /**
     * @return string
     */
    public function getOriginalPurchasePrice()
    {
        return $this->originalPurchasePrice;
    }

    /**
     * @param string $originalPurchasePrice
     */
    public function setOriginalPurchasePrice($originalPurchasePrice)
    {
        $this->originalPurchasePrice = $originalPurchasePrice;
    }

    /**
     * @return string
     */
    public function getSalesPrice()
    {
        return $this->salesPrice;
    }

    /**
     * @param string $salesPrice
     */
    public function setSalesPrice($salesPrice)
    {
        $this->salesPrice = $salesPrice;
    }

    /**
     * @return string
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * @param string $purchasePrice
     */
    public function setPurchasePrice($purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;
    }

    /**
     * @return Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param Supplier $supplier
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return string
     */
    public function getMinimumPurchaseQuantity()
    {
        return $this->minimumPurchaseQuantity;
    }

    /**
     * @param string $minimumPurchaseQuantity
     */
    public function setMinimumPurchaseQuantity($minimumPurchaseQuantity)
    {
        $this->minimumPurchaseQuantity = $minimumPurchaseQuantity;
    }
}

<?php
namespace OrderDirect\Entity;

class Relation implements EntityInterface
{
    /**
     * @var string
     */
    private $number;

    /**
     * @var string
     */
    private $shortName;

    /**
     * @var string
     */
    private $vatNumber;

    /**
     * @var string
     */
    private $cocNumber;

    /**
     * @var Currency
     */
    private $currency = null;

    /**
     * @var Language
     */
    private $language = null;

    /**
     * @var Email
     */
    private $email = null;

    /**
     * @var CustomerPayment
     */
    private $customerPayments;

    /**
     * @var PriceAgreement
     */
    private $priceAgreements;

    /**
     * @var string
     */
    private $active;

    /**
     * @var string
     */
    private $homepage;

    /**
     * @var Reminder
     */
    private $reminders;

    /**
     * @var string
     */
    private $created;

    /**
     * @var string
     */
    private $modified;

    /**
     * @var Address[]
     */
    private $addresses = [];

    /**
     * @var Contact[]
     */
    private $contacts = [];

    /**
     * @var CustomerCustomerGroup[]
     */
    private $customerCustomerGroups = [];

    /**
     * @var Relationship
     */
    private $relationship = null;

    /**
     * @var array
     */
    private $columns = ['number', 'shortName', 'vatNumber', 'cocNumber', 'active', 'created', 'modified', 'homepage'];

    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }

        if (isset($data['addresses']) && count($data['addresses']) > 0) {
            foreach ($data['addresses'] as $addressData) {
                $this->addresses[] = new Address($addressData['address']);
            }
        }

        if (isset($data['contacts']) && count($data['contacts']) > 0) {
            foreach ($data['contacts'] as $contactData) {
                $this->contacts[] = new Contact($contactData['contact']);
            }
        }

        if (isset($data['customerCustomerGroups']) && count($data['customerCustomerGroups']) > 0) {
            foreach ($data['customerCustomerGroups'] as $customerCustomerGroupsData) {
                $this->customerCustomerGroups[] = new CustomerCustomerGroup($customerCustomerGroupsData['customerCustomerGroup']);
            }
        }

        if (isset($data['email'])) {
            $this->email = new Email($data['email']);
        }

        if (isset($data['currency'])) {
            $this->currency = new Currency($data['currency']);
        }

        if (isset($data['language'])) {
            $this->language = new Language($data['language']);
        }

        if (isset($data['customerPayments'])) {
            $this->customerPayments = new CustomerPayment($data['customerPayments']);
        }

        if (isset($data['priceAgreements'])) {
            $this->priceAgreements = new PriceAgreement($data['priceAgreements']);
        }

        if (isset($data['reminders'])) {
            $this->reminders = new Reminder($data['reminders']);
        }

        if (isset($data['relationship'])) {
            $this->relationship = new Relationship($data['relationship']);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach ($this->columns as $column) {
            $data[$column] = $this->$column;
        }

        if (count($this->addresses) > 0) {
            $data['addresses'] = [];

            foreach ($this->addresses as $address) {
                $data['addresses'][]['address'] = $address->toArray();
            }
        }

        if (count($this->contacts) > 0) {
            $data['contacts'] = [];

            foreach ($this->contacts as $contact) {
                $data['contacts'][]['contact'] = $contact->toArray();
            }
        }

        if (count($this->customerCustomerGroups) > 0) {
            $data['customerCustomerGroups'] = [];

            foreach ($this->customerCustomerGroups as $customerCustomerGroup) {
                $data['customerCustomerGroups'][] = $customerCustomerGroup->toArray();
            }
        }

        if (!is_null($this->email)) {
            $data['email'] = $this->email->toArray();
        }

        if (!is_null($this->currency)) {
            $data['currency'] = $this->currency->toArray();
        }

        if (!is_null($this->language)) {
            $data['language'] = $this->language->toArray();
        }

        if (!is_null($this->customerPayments)) {
            $data['customerPayments'] = $this->customerPayments->toArray();
        }

        if (!is_null($this->priceAgreements)) {
            $data['priceAgreements'] = $this->priceAgreements->toArray();
        }

        if (!is_null($this->priceAgreements)) {
            $data['priceAgreements'] = $this->priceAgreements->toArray();
        }

        if (!is_null($this->reminders)) {
            $data['reminders'] = $this->reminders->toArray();
        }

        if (!is_null($this->relationship)) {
            $data['relationship'] = $this->relationship->toArray();
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @return string
     */
    public function getVatNumber()
    {
        return $this->vatNumber;
    }

    /**
     * @return string
     */
    public function getCocNumber()
    {
        return $this->cocNumber;
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return CustomerPayment
     */
    public function getCustomerPayments()
    {
        return $this->customerPayments;
    }

    /**
     * @return PriceAgreement
     */
    public function getPriceAgreements()
    {
        return $this->priceAgreements;
    }

    /**
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @return Reminder
     */
    public function getReminders()
    {
        return $this->reminders;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return string
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @return Address
     */
    public function getStandaardAdres()
    {
        foreach($this->addresses as $address) {
            if($address->getDescription() == Address::STANDAARD_ADRES) {
                return $address;
            }
        }
    }

    /**
     * @param Address $address
     */
    public function setStandaardAdres(Address $address)
    {
        foreach($this->addresses as $key => $currentAddress) {
            if($currentAddress->getDescription() == Address::STANDAARD_ADRES) {
                $this->addresses[$key] = $address;
            }
        }
    }

    /**
     * @return Address
     */
    public function getFactuurAdres()
    {
        foreach($this->addresses as $address) {
            if($address->getDescription() == Address::FACTUUR_ADRES) {
                return $address;
            }
        }
    }

    /**
     * @param Address $address
     */
    public function setFactuurAdres(Address $address)
    {
        foreach($this->addresses as $key => $currentAddress) {
            if($currentAddress->getDescription() == Address::FACTUUR_ADRES) {
                $this->addresses[$key] = $address;
            }
        }
    }

    /**
     * @return Contact
     */
    public function getFirstContact()
    {
        if(count($this->contacts) > 0) {
            return $this->contacts[0];
        }
    }

    /**
     * @return string
     */
    public function getBrancheDescription()
    {
        foreach($this->customerCustomerGroups as $customerGroup) {
            return $customerGroup->getCustomerGroup()->getDescription();
        }

        return '';
    }

    /**
     * @return Contact[]
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @return CustomerCustomerGroup[]
     */
    public function getCustomerCustomerGroups()
    {
        return $this->customerCustomerGroups;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @param string $shortName
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;
    }

    /**
     * @param string $vatNumber
     */
    public function setVatNumber($vatNumber)
    {
        $this->vatNumber = $vatNumber;
    }

    /**
     * @param string $cocNumber
     */
    public function setCocNumber($cocNumber)
    {
        $this->cocNumber = $cocNumber;
    }

    /**
     * @param Currency $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @param Language $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @param Email $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param CustomerPayment $customerPayments
     */
    public function setCustomerPayments($customerPayments)
    {
        $this->customerPayments = $customerPayments;
    }

    /**
     * @param PriceAgreement $priceAgreements
     */
    public function setPriceAgreements($priceAgreements)
    {
        $this->priceAgreements = $priceAgreements;
    }

    /**
     * @param string $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @param string $homepage
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @param Reminder $reminders
     */
    public function setReminders($reminders)
    {
        $this->reminders = $reminders;
    }

    /**
     * @param string $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @param string $modified
     */
    public function setModified($modified)
    {
        $this->modified = $modified;
    }

    /**
     * @param Address[] $addresses
     */
    public function setAddresses($addresses)
    {
        $this->addresses = $addresses;
    }

    /**
     * @param Contact[] $contacts
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;
    }

    /**
     * @param CustomerCustomerGroup[] $customerCustomerGroups
     */
    public function setCustomerCustomerGroups($customerCustomerGroups)
    {
        $this->customerCustomerGroups = $customerCustomerGroups;
    }

    /**
     * @param Relationship $relationship
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;
    }
}

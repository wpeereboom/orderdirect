<?php
namespace OrderDirect\Entity;

class Relationship implements EntityInterface
{

    const TYPE_CUSTOMER = 'customer';
    /**
     * @var array
     */
    private $types = [];

    /**
     * @var CustomerDepartment[]
     */
    private $customerDepartments = [];

    /**
     * @var array
     */
    private $columns = ['types',];

    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }

        if (isset($data['customerDepartments'])) {
            $this->customerDepartments[] = new CustomerDepartment($data['customerDepartments']);
        }
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }


    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        if(!empty($this->types)) {
            $data['types'][] = $this->types;
        }

        if (!empty($this->customerDepartments)) {
            $data['customerDepartments'] = [];

            foreach($this->customerDepartments as $customerDepartment) {
                $data['customerDepartments'][] = $customerDepartment->toArray();
            }
        }

        return $data;
    }

    /**
     * @return CustomerDepartment[]
     */
    public function getCustomerDepartments()
    {
        return $this->customerDepartments;
    }

    /**
     * @param array $types
     */
    public function setTypes($types)
    {
        $this->types = $types;
    }
}

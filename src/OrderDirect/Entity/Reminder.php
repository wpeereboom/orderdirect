<?php
namespace OrderDirect\Entity;

class Reminder implements EntityInterface
{
    /**
     * @var string
     */
    private $sendByEmail;

    /**
     * @var string
     */
    private $useEmailAddress;

    /**
     * @var string
     */
    private $sendToAccountsPayable;

    /**
     * @var Email
     */
    private $email;

    /**
     * @var array
     */
    private $columns = ['sendByEmail', 'useEmailAddress', 'sendToAccountsPayable'];

    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }

        if (isset($data['email'])) {
            $this->email = new Email($data['email']);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            $data[$column] = $this->$column;
        }


        if (!is_null($this->email)) {
            $data['email'] = $this->email->toArray();
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getUseEmailAddress()
    {
        return $this->useEmailAddress;
    }

    /**
     * @return string
     */
    public function getSendByEmail()
    {
        return $this->sendByEmail;
    }

    /**
     * @return string
     */
    public function getSendToAccountsPayable()
    {
        return $this->sendToAccountsPayable;
    }

    /**
     * @return Email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param string $sendByEmail
     */
    public function setSendByEmail($sendByEmail)
    {
        $this->sendByEmail = $sendByEmail;
    }

    /**
     * @param string $useEmailAddress
     */
    public function setUseEmailAddress($useEmailAddress)
    {
        $this->useEmailAddress = $useEmailAddress;
    }

    /**
     * @param string $sendToAccountsPayable
     */
    public function setSendToAccountsPayable($sendToAccountsPayable)
    {
        $this->sendToAccountsPayable = $sendToAccountsPayable;
    }

    /**
     * @param Email $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }
}

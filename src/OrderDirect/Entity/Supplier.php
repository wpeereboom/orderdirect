<?php
namespace OrderDirect\Entity;

class Supplier implements EntityInterface
{

    /**
     * @var string
     */
    private $number;


    /**
     * @var array
     */
    private $columns = ['number'];

    /**
     * Manufacturer constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            $data[$column] = $this->$column;
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }
}

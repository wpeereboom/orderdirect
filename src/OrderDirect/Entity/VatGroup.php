<?php
namespace OrderDirect\Entity;

class VatGroup implements EntityInterface
{
    /**
     * @var int
     */
    private $number;

    /**
     * @var int
     */
    private $percentage;

    /**
     * @var array
     */
    private $columns = ['number', 'percentage'];

    /**
     * VatGroup constructor.
     * @param array $data
     */
    public function __construct($data)
    {
        foreach ($this->columns as $column) {
            if (isset($data[$column])) {
                $this->$column = $data[$column];
            }
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $data = [];

        foreach($this->columns as $column) {
            $data[$column] = $this->$column;
        }

        return $data;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return int
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param int $percentage
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;
    }
}

<?php
namespace OrderDirect\Repository;


use OrderDirect\Entity\Relation;
use OrderDirect\Exception\NotFoundException;

class RelatieRepository extends RepositoryAbstract
{

    public function get($number)
    {
        $relatieResult = $this->client->getRelatie($number);

        if(is_null($relatieResult)) {
            throw new NotFoundException('Relation niet gevonden');
        }

        $relatieObj = $relatieResult->relatie;

        return new Relation(
            $relatieObj->nummer,
            $relatieObj->naam,
            $relatieObj->straat,
            $relatieObj->postcode,
            $relatieObj->plaats,
            $relatieObj->landcode,
            $relatieObj->telefoon,
            $relatieObj->email
        );
    }
} 

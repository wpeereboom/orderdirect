<?php
namespace OrderDirect\Repository;

use OrderDirect\Entity\RelatieProperties;

class RelatiePropertiesRepository extends RepositoryAbstract
{

    public function get($nummer)
    {
        $result = $this->client->getRelatieProperties($nummer);

        if(is_null($result)) {
            throw new NotFoundException('RelatieProperties niet gevonden');
        }

        return new RelatieProperties(
            $result->naamPlaats,
            $result->countContact,
            $result->countAddress
        );
    }
} 

<?php
namespace OrderDirect\Repository;

use OrderDirect\Client;

class RepositoryAbstract {

    /**
     * @var \OrderDirect\Client
     */
    protected $client;

    public function __construct(Client $client){

        $this->client = $client;
    }

} 

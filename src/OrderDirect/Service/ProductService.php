<?php
namespace OrderDirect\Service;

use OrderDirect\Client;
use OrderDirect\Collection\ProductCollection;
use OrderDirect\Entity\EntityInterface;
use OrderDirect\Entity\Product;

class ProductService implements ServiceInterface
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function save(EntityInterface $entity)
    {
        if($entity->getNumber() != '') {
            return $this->update($entity);
        } else {
            return $this->create($entity);
        }
    }

    /**
     * @param int $id
     * @return Product
     */
    public function find($id)
    {
        $data = $this->client->get('product', $id);

        return new Product($data['product']);
    }

    public function getAll()
    {
        $data = $this->client->get('products');

        return new ProductCollection($data['products']);
    }

    public function update($entity)
    {
        return $this->client->update('product', $entity);
    }

    public function create($entity)
    {
        return $this->client->create('product', $entity);
    }
}

<?php
namespace OrderDirect\Service;

use OrderDirect\Client;
use OrderDirect\Entity\EntityInterface;
use OrderDirect\Entity\Relation;

class RelationService implements ServiceInterface
{
    /**
     * @var Client
     */
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function save(EntityInterface $entity)
    {
        if($entity->getNumber() != '') {
            return $this->client->update('relation', $entity);
        } else {
            return $this->client->create('relation', $entity);
        }
    }

    /**
     * @param int $id
     * @return Relation
     */
    public function find($id)
    {
        $data = $this->client->get('relation', $id);

        $relation = new Relation($data['relation']);

        return $relation;
    }
}

<?php
namespace OrderDirect\Service;

use OrderDirect\Entity\EntityInterface;

interface ServiceInterface
{

    public function save(EntityInterface $entity);
}
